#####################################################################################
#                    Dockerfile for the trading app                                 #
#####################################################################################

#Use openjdk 11 as our base image (java support)
FROM openjdk:11-jdk
ARG JAR_FILE=/target/trading-app.jar
COPY ${JAR_FILE} trading-app.jar

#run the jar file, which starts our spring boot app
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/trading-app.jar"] 

#Just a reminder to expose port 8080 when running
EXPOSE 8080

#setup (internal) container health check
HEALTHCHECK CMD curl --fail localhost:8080/health || exit 1