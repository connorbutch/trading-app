package com.connor.tradingapp.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.slf4j.Logger;
import org.springframework.boot.actuate.health.Health;import org.springframework.boot.info.BuildProperties;

import com.connor.tradingapp.model.VersionInfo;

@ExtendWith(MockitoExtension.class)
public class RequestControllerTest {

	@Mock
	private Logger log;
	
	@Mock
	private BuildProperties buildProperties;
	
	@InjectMocks
	private RequestController requestController;
	
	@Test
	public void healthCheckShouldReturnUp() {
		Health healthFromController = requestController.health();
		assertNotNull("Health should not be null", healthFromController);
		assertEquals("Health status should be up", healthFromController, Health.up().build());
	}	
	
	@Test
	public void infoRequestShouldReturnInfo() {
		final String artifactId = "trading-app";
		final String version = "0.0.1-SNAPSHOT";
		final Instant buildTimestamp = Instant.now();
		Mockito.when(buildProperties.getArtifact()).thenReturn(artifactId);
		Mockito.when(buildProperties.getVersion()).thenReturn(version);
		Mockito.when(buildProperties.getTime()).thenReturn(buildTimestamp);
		
		VersionInfo versionInfoReturned = requestController.getVersionInfo();
		assertEquals("Artifact id should match the value from build info", artifactId, versionInfoReturned.getArtifactId());
		assertEquals("Version should equal value from build info", version, versionInfoReturned.getVersion());
		assertEquals("Build timestamp should match value from build info", buildTimestamp, versionInfoReturned.getBuildTimestamp());
	}
}