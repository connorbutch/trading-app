package com.connor.tradingapp.controller;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.boot.info.BuildProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.connor.tradingapp.model.VersionInfo;
import com.connor.tradingapp.service.TradeService;

/**
 * This is the controller class that handles all incoming requests.
 * @author connor
 *
 */
@RestController()
public class RequestController implements HealthIndicator {

	private final Logger log;		
	
	private final BuildProperties buildProperties;
	
	private final TradeService tradeService;
	
	@Autowired
	public RequestController(Logger log, BuildProperties buildProperties, TradeService tradeService) {
		this.log = log;
		this.buildProperties = buildProperties;
		this.tradeService = tradeService;
	}

	@Override
	public Health health() {
		log.debug("Recieved health check request");
		return Health.up().build(); //spring boot is smart enough to override our code for "up" if something is down (such as cannot do dummy select from database).  Even if app is up, this will show down.
	}
	
	@GetMapping("/version")
	public VersionInfo getVersionInfo() {
		log.debug("Recieved request for build information");
		
		VersionInfo versionInfo = new VersionInfo();
		versionInfo.setArtifactId(buildProperties.getArtifact());
		versionInfo.setBuildTimestamp(buildProperties.getTime());
		versionInfo.setVersion(buildProperties.getVersion());
		
		log.debug("Returning {} for version information", versionInfo);
		return versionInfo;
	}
	
	@GetMapping("/dummy")
	public Integer dummy() {
		return tradeService.dummy();
	}
}