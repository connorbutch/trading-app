package com.connor.tradingapp.cdi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration 
public class CdiProducer {

	/**
	 * Producer meethod for logger.
	 * @param ip
	 * @return
	 */
	@Bean
	public Logger getLogger(InjectionPoint ip) {
		return LoggerFactory.getLogger(getClassForLoggerBasedOnInjectionPoint(ip));
	}
	
	
	protected Class<?> getClassForLoggerBasedOnInjectionPoint(InjectionPoint ip){
		Class<?> clazz = CdiProducer.class; //should never happen . . . helps make unit testing easier
		if(ip != null) {
			if(ip.getField() != null) {
				clazz = ip.getField().getType();
			}else if(ip.getMethodParameter() != null) {
				clazz = ip.getMethodParameter().getParameterType();
			}
		}		
		return clazz;
	}	
}