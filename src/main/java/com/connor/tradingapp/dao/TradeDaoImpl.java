package com.connor.tradingapp.dao;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class TradeDaoImpl implements TradeDao {

	private final Logger log;
	
	private final NamedParameterJdbcTemplate namedTemplate;	
	
	@Autowired
	public TradeDaoImpl(Logger log, NamedParameterJdbcTemplate namedTemplate) {
		this.log = log;
		this.namedTemplate = namedTemplate;
	}

	@Override
	public int dummy() {
		return namedTemplate.queryForObject("SELECT COUNT(1) FROM ASSESSMENT", new MapSqlParameterSource(), Integer.class);
	}

}
