package com.connor.tradingapp.advice;


import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ResponseEntityExceptionHandlerImpl extends ResponseEntityExceptionHandler {	
	
	private final Logger log;

	@Autowired
	public ResponseEntityExceptionHandlerImpl(Logger log) {
		this.log = log;
	}

	@Override
	protected ResponseEntity<Object> handleExceptionInternal(
			Exception e, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {		
		log.error("Handling an unexpected exception", e);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);		
	}	
}