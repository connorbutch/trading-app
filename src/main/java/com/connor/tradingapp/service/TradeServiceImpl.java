package com.connor.tradingapp.service;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.connor.tradingapp.dao.TradeDao;

@Service
public class TradeServiceImpl implements TradeService {

	private final Logger log;
	
	private final TradeDao tradeDao;
	
	
	@Autowired
	public TradeServiceImpl(Logger log, TradeDao tradeDao) {
		this.log = log;
		this.tradeDao = tradeDao;
	}



	@Override
	public int dummy() {
		return tradeDao.dummy();
	}

}
