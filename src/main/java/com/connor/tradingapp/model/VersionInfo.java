package com.connor.tradingapp.model;

import java.io.Serializable;
import java.time.Instant;


public class VersionInfo implements Serializable {
	
	private static final long serialVersionUID = 12341982734123424L;
	
	private String version;
	
	private Instant buildTimestamp;
	
	private String artifactId;

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Instant getBuildTimestamp() {
		return buildTimestamp;
	}

	public void setBuildTimestamp(Instant buildTimestamp) {
		this.buildTimestamp = buildTimestamp;
	}

	public String getArtifactId() {
		return artifactId;
	}

	public void setArtifactId(String artifactId) {
		this.artifactId = artifactId;
	}
}