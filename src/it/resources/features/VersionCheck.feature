#Author: Connor Butch
Feature: Version check

#NOTE: to make this as deterministic as possible, we just check the build timestamp will be populated
  Scenario: Version check should return the applicable information
    Given The server is started
    When a version check request is made   
    Then the artifact id in the response will be trading-app
    And the version in the response will match the version in the pom
    And the build timestamp will be populated    