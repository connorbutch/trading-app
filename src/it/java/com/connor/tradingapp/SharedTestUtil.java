package com.connor.tradingapp;

import java.util.Optional;

public class SharedTestUtil {

	private static final String BASE_URI_SYS_PROPERTY = "BASE_URL";

	public String getBaseUriThrowExceptionIfNull() {
		return getBaseUri()
				.orElseThrow(() -> new RuntimeException(String.format("Please set the %s system property", BASE_URI_SYS_PROPERTY)));
	}

	protected Optional<String> getBaseUri(){
		return Optional.ofNullable(System.getProperty(BASE_URI_SYS_PROPERTY));
	}	
}