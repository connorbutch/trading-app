package com.connor.tradingapp.version;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class) 
@CucumberOptions(features = "src/it/resources/features/VersionCheck.feature", plugin = {"json:target/version-check/cucumber.json", "html:target/version-check/html"}, glue = {"com.connor.tradingapp.version"})
public class RunVersionCheckIT {
	//intentionally blank
}
