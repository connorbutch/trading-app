package com.connor.tradingapp.version;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.URI;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import com.connor.tradingapp.SharedTestUtil;
import com.connor.tradingapp.model.VersionInfo;

import io.cucumber.java8.En;

public class VersionCheckGWT implements En {
	
	private static final String VERSION_FORMAT_STR = "%s/version";

	private final TestRestTemplate testRestTemplate = new TestRestTemplate();

	private final SharedTestUtil sharedTestUtil = new SharedTestUtil();

	private ResponseEntity<VersionInfo> response;
	
	public VersionCheckGWT() {
		Given("The server is started", () -> {
			//intentionally blank
		});

		When("a version check request is made", () -> {
			response = testRestTemplate.getForEntity(getURIForVersionCheckRequest(), VersionInfo.class);	
		});

		Then("the artifact id in the response will be trading-app", () -> {
			assertEquals("Artifact id should match expected artifact id", "trading-app", response.getBody().getArtifactId());
		});

		Then("the version in the response will match the version in the pom", () -> {
			final String expectedVersion = "0.0.1-SNAPSHOT"; //TODO get version from pom
			assertEquals("Version should match", expectedVersion, response.getBody().getVersion());
		});

		Then("the build timestamp will be populated", () -> {
			assertNotNull("Build timestamp should not be null", response.getBody().getBuildTimestamp());
		});
	}

	protected URI getURIForVersionCheckRequest() {
		return URI.create(String.format(VERSION_FORMAT_STR, sharedTestUtil.getBaseUriThrowExceptionIfNull()));
	}
}