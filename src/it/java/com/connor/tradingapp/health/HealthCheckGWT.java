package com.connor.tradingapp.health;


import static org.junit.Assert.assertEquals;

import java.net.URI;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.connor.tradingapp.SharedTestUtil;

import io.cucumber.java8.En;

public class HealthCheckGWT implements En {	

	private static final String HEALTH_CHECK_URI_FORMAT_STR = "%s/health";

	private final TestRestTemplate testRestTemplate = new TestRestTemplate();

	private final SharedTestUtil sharedTestUtil = new SharedTestUtil();

	private ResponseEntity<?> response;

	public HealthCheckGWT() {
		Given("The server is started", () -> {
			//intentionally blank -- this is just a placeholder for readability of feature file
		});

		When("a health check request is made", () -> {
			//cannot get as type (health) because health doesn't have default constructor
			response = testRestTemplate.getForEntity(getURIForHealthCheckRequest(), String.class);	
		});

		Then("the response will indicate the server is healthy", () -> {
			assertEquals("Should get a 200 response on a health check", HttpStatus.OK, response.getStatusCode());
			assertEquals("Status body should denote up", "{\"status\":\"UP\"}", response.getBody());
		});
	}

	protected URI getURIForHealthCheckRequest() {
		return URI.create(String.format(HEALTH_CHECK_URI_FORMAT_STR, sharedTestUtil.getBaseUriThrowExceptionIfNull()));
	}
}