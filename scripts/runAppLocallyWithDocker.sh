#################################################################################################
# This file can be used to run the app locally in Docker, and get the ip address of the service #
#################################################################################################


#print out the value for debug.  Only prints if debug system variable value  is true
debug()
{
 if [ "$debug" == "true" ]; then
 	echo $1
 fi
}

#The -D can be a pain to put inline check, so just run individually, then check return code
debug 'Attempting to run maven build'
mvn clean install -DskipTests &> /dev/null
if [ $? -ne 0 ]
then
    debug 'Failed to run maven build'
    exit 1
else
    debug 'Successfully ran maven build'
fi

debug 'Attempting to build docker image'
if [ docker build . -t connorbutch/trading-app:latest &> /dev/null -ne 0 ]
then
    debug 'Failed to build docker image'
    exit 2
else
    debug 'Successfully built docker image'
fi

debug 'Attempting to remove container with same name to avoid conflicts, if exists'
docker rm trading-app &> /dev/null
debug 'Successfully removed container with same name if one existed'

idOfContainerRunningOnPort8080=$(docker ps | grep ":8080" | awk '{print $1}') 
if [ -z "$idOfContainerRunningOnPort8080" ]
then
      debug 'There is no container running on port 8080'
else
	  debug 'Attempting to stop container running on port 8080'
      if [ docker stop $idOfContainerRunningOnPort8080 &> /dev/null -ne 0 ]
      then
      	debug 'Failed to stop container running on port 8080'
      	exit 3
      else
        debug 'Successfully stopped container running on port 8080'
      fi      
fi

debug 'Attempting to run docker image'
docker run -d -e "DB_USERNAME=A" -e "DB_PASSWORD=B" -e "DB_JDBC_URL=jdbc:mysql://trading-app-db:3306/trade"--name trading-app connorbutch/trading-app:latest &> /dev/null
if [ $? -ne 0 ]
then
    debug 'Failed to run docker image'
    exit 4
else
    debug 'Successfully ran docker image'
fi

debug 'The ip of the image is: '
docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' trading-app