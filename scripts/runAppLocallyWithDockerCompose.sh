#################################################################################################
#            This file can be used to run the app locally in Docker-compose                     #
#################################################################################################

#NOTE: This file is not used in production, only for integration tests; and the container always run on localhost:8080


#print out the value for debug.  Only prints if debug system variable value  is true
debug()
{
# if [ "$debug" == "true" ]; then
 	echo $1
# fi
}


#The -D can be a pain to put inline check, so just run individually, then check return code
debug 'Attempting to run maven build'
mvn clean install -DskipTests &> /dev/null
if [ $? -ne 0 ]
then
    debug 'Failed to run maven build'
    exit 1
else
    debug 'Successfully ran maven build'
fi

debug 'Attempting to build docker image'
docker build . -t connorbutch/trading-app &> /dev/null
if [ $? -ne 0 ]
then
    debug 'Failed to build docker image'
    exit 2
else
    debug 'Successfully built docker image'
fi

debug 'Doing docker volume cleanup . . .'
yes | docker volume prune &> /dev/null
if [ $? -ne 0 ]
then
	debug 'Failed to prune unused docker volumes'
	exit $?
else
	debug 'Successfully pruned unused docker volumes'
fi

debug 'Killing running docker-compose'
docker-compose down -v &> /dev/null
if [ $? -ne 0 ]
then
	debug 'Failed to take docker compose down'
	exit $?
else
	debug 'Successfully killed preexisting docker compose stuff'
fi

debug 'Running docker compose build'
docker-compose build --parallel --no-cache --force-rm &> /dev/null
if [ $? -ne 0 ]
then
	debug 'Failed to docker-compose build'
	exit $?
else
	debug 'Successfully docker-compose build'
fi

debug 'Attempting to run docker compose locally'
DB_USERNAME=A DB_PASSWORD=B docker-compose up -d &> /dev/null
if [ $? -ne 0 ]
then
	debug 'Failed to run locally'
	exit $?
else
	debug 'Successfully ran docker compose locally'
fi